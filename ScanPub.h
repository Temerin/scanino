
#include <arduino2.h> //Для оптимального переключения пинов
#include <Ethernet.h>
#include <Encoder.h> //Для работы с энкодерами
#include <TimerOne.h> //таймеры
#include <stdarg.h> //нет необходимости. Нужно было для отладки
int engPinStep[2] = {23,22};  //Распиновка. Двигатели. Каждый двигатель должен соответствовать своему энкодеру.
int engPinDir[2] = {20,24};
int engPinEn[2] = {27,46};
int pinOpen[2] = {29,28}; //Концевые выключатели.

int inSize= 0; // Переменная которая будет содержать размер буфера.
char str1[256]; // Так как типа string тут нет, будем использовать массив символов.
char* str; //Указатель на наш массив.
byte axis; // ось. 0-ось не выбрана.
long int par[5];
long int destination[2]={0,0}; //Конечная точка.
boolean forwardDiretion[2]={1,1}; //Направление движения.
boolean tagFind[2]={0,0}; //Идет поиск опорной метки.
long posSw[2] = {0,0}; //Поправка позиции по энкодеру.
long stopLong[2] = {2000,2000};//Базовое расстояние торможения.
int refLongDiv[2] = {1000,1000}; //Базовое количество тактов таймера на каждой из частот.
int refDivider[2] = {16,16}; //Базовый делитель частоты.
int pinSync = 9;
long int disPos[2]={0,0}; //Текущий дискрет позции.
int numDisPos[2]={0,0}; //Делитель позиции, при достижении которого будет подаваться импульс на выход.
int prevDisPos[2]={0,0};

int deltaPos[2]={10,10}; //При разности текущей позиции с заданной меньше, чем на эту велечину, ось никуда не едет.

int longDiv[2] = {0,0}; //Количество тактов таймера на каждой из частот.
int divider[2] = {1,1}; //Текущий делитель.
int statDiv[2] = {0,0}; //При превышении этой переменной делителя, подается импульс на двигатель.
int sLongDiv[2] = {0,0}; //Текущий такт по счету на этой частоте.
int inc[2] = {1,1}; //Переменная для увеличения или уменьшения делителя. Принимает значения 1/-1

EthernetClient client;


byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);
IPAddress subnet(255, 255, 255, 0);

EthernetServer server(400);
boolean alreadyConnected = false; // whether or not the client was connected previously

class Engine //Класс "Двигатель"
{	
	int stepPin; //Пин для подачи импульсов.
	int dirPin; //Пин направления.
	int enPin; //Пин питания.
	int engState; //Питание на stepPin.
	byte number; //Номер двигателя.
	boolean inv; //Инвертированно ли направление двигателя.
	unsigned long previousMillis; //Предыдущее время в миллисекундах.
	unsigned long currentMillis; // Текущее время в миллисекундах.

	public: 
    byte stat; //Битовое поле состояния.
    boolean power; //Питание (state).
    byte erCode; //Код ошибки.
	volatile long int pos; //Позиция по количеству импульсов на двигатель. В этом сканере не используется, т.к. ориентируемся на энкодеры.
  
	Engine(int pin1, int pin2, int pin3, byte num, boolean invert=0, byte st=0, byte erC=0) //распиновка, номер двигателя, направление, состояние, код ошибки
	{
		stepPin = pin1; //Присваиваем полученые пины двигателю.
		dirPin = pin2;
		enPin = pin3;
		number = num; //Даем знать двигателю свой номер. Нужно для простоты реализации некоторых возможностей.
		pinMode2(stepPin, OUTPUT); //устанавливаем пины двигателя в выходной режим.
		pinMode2(dirPin, OUTPUT);
		pinMode2(enPin, OUTPUT);
		previousMillis = millis(); //Запоминаем ориентировочное время.
		SetPower(0); //Выключаем питание двигателю (по умолчанию Arduino питает все пины)
		stat = st; //Установка поля состояния.
		erCode = erC; //Установка кода ошибки.
		power=0; //Питание выключено.
		pos=0; //Двигатель условно в нуле.
		inv=invert; //Инвертируем двигатель, если установлен наоборот.
	}

	void SetPower(boolean p) //установка питания
	{
		power = p; 
		if(p) digitalWrite2(enPin, HIGH);
		else digitalWrite2(enPin, LOW);
	}
	
	void SetErCode(byte erC) //установка кода ошибки
	{
		stat = 1;
		erCode = erC;
	}
	
	void SetStat(byte st) //установка состояния двигателя
	{
		stat = st;
	}
	
	void Move(boolean forDir) //направление (true = вперед, false = назад), задержки заданы по-умолчанию, но возможно переназначение по ситуации (Уже нет. Другая реализация.)
	{ 
		forDir=forDir ^ inv; //Если двигатель инвертирован, то направление меняется на противоположное.
		if(forDir) digitalWrite2(dirPin, LOW); //Задание направления.
		else digitalWrite2(dirPin, HIGH);
		//currentMillis = millis(); // Текущее время в миллисекундах. Фактически, сейчас не используется. Пришлось отказаться от этой задумки для большей скорости.
		if(engState == HIGH) //смена состояния stepPin (подача импульса двигателю).
		{   
			engState = LOW; // выключаем 
			digitalWrite2(stepPin, LOW);// реализуем новое состояние
		} 
		else if (engState == LOW) 
		{ 	  
			if(((forDir)&&(!inv)) || ((!forDir)&&(inv))) pos--; //отсчет движения в зависимости от направления. В этой версии - не имеет знавчения.
			else pos++;
			engState = HIGH; // выключаем 
			digitalWrite2(stepPin, HIGH); 
		} 
	}
};

Encoder enc[2] = 
{
	Encoder(3,18), //создание 4х энкодеров и четырех двигателей в соответствии с распиновкой.
	Encoder(19,2),
};

Engine eng[2] = 
{
	Engine(engPinStep[0],engPinDir[0],engPinEn[0],1,1),//распиновка, номер двигателя. 1 после номера, если направление обратное.
	Engine(engPinStep[1],engPinDir[1],engPinEn[1],2),
};

void setup() 
{
	pinMode2(pinSync, OUTPUT);
	// initialize the ethernet device
	Ethernet.begin(mac, ip, subnet);
	// start listening for clients
	server.begin();
  
	// Open serial communications and wait for port to open:
  
	Timer1.initialize(); //Инициализация таймера
	Timer1.setPeriod(120); //Частота таймера. Точная формула потерялась, но чем меньше значения (*), тем больше частота. 
	Timer1.attachInterrupt( timerIsr ); //Создание функции прерывания по таймеру.
  
	for(byte i=0; i<2; i++) //Установка пинов концевых выключателей в состояние входа.
	{
		pinMode2(pinOpen[i], INPUT);
	}
}

void p(char *fmt, ... )//Нужно было для отладки. Сейчас не используется.
{
        char buf[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        client.print(buf);
}

void Idn() //далее идут команды. Смотреть ТЗ. 
{
	client.print("Model: ");
	client.println("Arduino MEGA, BIG");
	client.print("ver: ");
	client.println("5.21");
}

void StSet(byte ax=0, byte s=0, byte c=0) //дефолтно работает как Clear. 
{
	if(!ax) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetStat(s);
			eng[j].SetErCode(c);
		}
	}
	else 
	{
		eng[ax-1].SetStat(s);
		eng[ax-1].SetErCode(c);
	}
}

void EnRead(byte p=0)  //Считывание состояния питания.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
	
		client.print(eng[0].power);
		client.print(",");
		client.print(eng[1].power);
		client.print("\n");		
		//client.println(eng[3].power);
	}
	else client.println(eng[p-1].power);
}

void En(byte p=0, boolean ch=0) //Установка питания.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetPower(ch);
		}
	}
	else eng[p-1].SetPower(ch);;
}

void Stat(byte p=0) //Вывод битового поля состояния.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{		
		byte stat=eng[0].stat;
		client.print(stat);
		client.print(",");
		byte stat1=eng[1].stat;
		client.print(stat1);
		client.print("\n");
	}
	else {  byte stat=eng[p-1].stat; client.println(stat); }
}

void Err(byte p=0) //Вывод ошибки
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
	
		byte code=eng[0].erCode;
		client.print(code);
		client.print(",");
		byte code1=eng[1].erCode;
		client.print(code1);
		client.print("\n");		
	}
	else {byte code=eng[p-1].erCode; client.println(code); }
}

void Clr(byte p=0) //Команда для отчистки от ошибок и состояния. Не обязательна. Сделана для простоты поддержки кода.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{			
			eng[j].SetErCode(0);
			eng[j].SetStat(0);
		}
	}
	else 
	{
		eng[p-1].SetErCode(0);
		eng[p-1].SetStat(0);
	}
}

void SetError(byte ch, byte p=0) //Установка ошибки.
{
	if(!p) //Если ось не указана, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetErCode(ch);
			eng[j].SetStat(1);
		}
	}
	else 
	{
		eng[p-1].SetErCode(ch);
		eng[p-1].SetStat(1);
	}
}

void FindTag(byte p) //поиск опорной метки
{
	if(!p) //Если ось не указано, то применяется ко всем. Может вызвать зависание, т.к. Arduino не справляется.
	{
		for (byte j=0; j<=1; j++)
		{
			bool fd=1; //Для направления движения. Можно и напрямую выставить.
			if(!(digitalRead2(pinOpen[j]))) //Если уже на концевом выключателе, то нужно отъехать от него.
			{
				fd=0; //Движемся вперед.
				destination[j]=9999999999999;
			}
			else destination[j]=-9999999999999; //Движемся назад (к концевику).
			longDiv[j]=refLongDiv[j]; //Возврат переменных разгона к эталонным.
			divider[j]=refDivider[j];
			forwardDiretion[j]=fd; //Установка направления.
			eng[j].SetStat(4); //Установка поля состояния в "Движение".
			tagFind[j]=1; //Идет поиск опорной метки.
		}
	}
	else 
	{
		bool fd=1; //Для направления движения. Можно и напрямую выставить.
		if(!(digitalRead2(pinOpen[p-1]))) //Если уже на концевом выключателе, то нужно отъехать от него.
		{
			fd=0; //Движемся вперед.
			destination[p-1]=9999999999999;
		}
		else destination[p-1]=-9999999999999;  //Движемся назад (к концевику).
		longDiv[p-1]=refLongDiv[p-1]; //Возврат переменных разгона к эталонным.
		divider[p-1]=refDivider[p-1];
		forwardDiretion[p-1]=fd; //Установка направления.
		eng[p-1].SetStat(4); //Установка поля состояния в "Движение".
		tagFind[p-1]=1; //Идет поиск опорной метки.
	}
}
void MoveTo(byte p) //движение с выбранной осью. Тут выставляется направление, а само движение идет по таймеру.
{
	long int posit=enc[p-1].read()-posSw[p-1]; //Считывание позиции с поправкой.
	  int bobPos=destination[p-1]-posit;
	  bool m = 0;
	  abs(bobPos);
	if(bobPos>deltaPos[p-1])//Если заданная позиция выходит за пределы дельты
	{
		divider[p-1]=refDivider[p-1];//Возврат переменных разгона к эталонным.
		if(posit>destination[p-1]) //В зависимости от текущей и заданной позиций...
		{
			if(numDisPos[p-1]>1) //Начиная с numDisPos==2, при старте должен подаваться импульс
			{
				if(numDisPos[p-1]>2)
				{
					disPos[p-1]=destination[p-1]-posit;
					disPos[p-1]=disPos[p-1]/(numDisPos[p-1]-1);
				}
				m=1;
			}
			forwardDiretion[p-1]=1;		//выбираем направление движения
		}
		else if(posit<destination[p-1])
		{
			if(numDisPos[p-1]>1)
			{
				if(numDisPos[p-1]>2)
				{
					disPos[p-1]=destination[p-1]+posit;
					disPos[p-1]=disPos[p-1]/(numDisPos[p-1]-1);
				}
				m=1;				
			}
			forwardDiretion[p-1]=0;			
		}
		eng[p-1].SetStat(4); //Установка поля состояния в "Движение".
		prevDisPos[p-1]=posit;
		if(m) 
		{    
			digitalWrite2(pinSync,HIGH);//Посыл импульса на синхро-выход
			prevDisPos[p-1]=posit;
			digitalWrite2(pinSync,LOW);
		}
	}
	else eng[p-1].SetStat(3);
		
}

void Abort(byte p=0) 
{
	if(!p)
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetStat(0); //останавливаем все действия. Сброс.
		}
	}
	else eng[p-1].SetStat(0);
}

void Pos(byte p=0) //Вывод позиции.
{
	if(!p)
	{
		
		long int g=enc[0].read()-posSw[0]; //Считывание позиции с поправкой.
		client.print(g);
		client.print(",");
		long int g1=enc[1].read()-posSw[1]; 
		client.print(g1);
		 client.print("\n");
	}
	else 
	{
		long int g=enc[p-1].read()-posSw[p-1]; //Считывание позиции с поправкой.
		client.println(g); 
	}
}

void Pulse(byte p=0, int num=0)
{
	disPos[p-1]=0;
	prevDisPos[p-1]=eng[p-1].pos;
	numDisPos[p-1]=num;
}


boolean Command() //тут происходит вызов всех команд. Лучше даже не заглядывать. 
{	
	if(par[0]>2) SetError(5);//Осей всего 4. Больше быть не должно.
	boolean error=true; //Параметр, отвечающий за успешность распознания команды.
	byte param = 0;//Если ось не выбрана.
	if (!par[0]) byte param=eng[par[0]-1].stat;//смотрим, что у нас за состояние выбронной оси.
	if (param!=1) //Ось не должна быть в состоянии ошибки. Иначе некоторые команды запрещены к вводу.
	{
		if(strcmp(str,"IDN?")==0)
		{
			error=false;		
			Idn();
		}
		if((strcmp(str,"EN?")==0)||(strcmp(str,"EN*?")==0))
		{
			error=false;		
			EnRead(par[0]);			
		}
		if((strcmp(str,"EN* *")==0)||(strcmp(str,"EN* *\n")==0))//!!!!
		{
			error=false;
			//client.println(par[1]);
			if(par[1]<2) //STATE 0 или 1.
			{
				En(par[0],par[1]);
			}
			else {error=false; eng[par[0]].SetErCode(4);} //ошибка 4
		}
		if((strcmp(str,"EN *,*\r")==0)||(strcmp(str,"EN *,*")==0))
		{
			error=false;
			for(int axis=0; axis<1; axis++)
			{
				if(par[axis+1]<3) //STATE 1 или 2.
				{
					En(axis,par[axis+1]);
				}
				else { eng[axis].SetErCode(4);} //ошибка 4
			}
		}
		if(strcmp(str,"PULSE* *")==0)
		{
			error=false;
			Pulse(par[0], par[1]);
		}
		if((strcmp(str,"MH")==0)||(strcmp(str,"MH*")==0))// двигаться, пока не достигнем опорной метки
		{
			error=false;
			FindTag(par[0]);
		}
		if((strcmp(str,"POS?")==0)||(strcmp(str,"POS*?")==0))
		{
			error=false;
			Pos(par[0]);
		}
		if((strcmp(str,"MOVE* *")==0))// для одной оси
		{
			error=false;
			destination[par[0]-1]=par[1];			
			MoveTo(par[0]);
		}
		if((strcmp(str,"MOVE *,*,*,*")==0)) //для всех осей
		{
			error=false;
			for(byte g=0; g<=1; g++)
			{
				destination[g]=par[g+1];
			}	
			MoveTo();
		}
	}
	//Команды, которые поддерживаются даже в состоянии ошибки.	
	if((strcmp(str,"CLR")==0)||(strcmp(str,"CLR*")==0))
	{
		error=false;	
		Clr(par[0]);			
	}
	if((strcmp(str,"ABORT")==0)||(strcmp(str,"ABORT*")==0))
	{
		error=false;	
		Abort(par[0]);
	}
	if((strcmp(str,"ERR?")==0)||(strcmp(str,"ERR*?")==0))
	{
		error=false;
		Err(par[0]);
	}
	if((strcmp(str,"STAT?")==0)||(strcmp(str,"STAT*?")==0))
	{
		error=false;
		Stat(par[0]);
	}
	return error;
}

void AnalCom() //кажется завершенной. Ошибки применяются ко всем осям, т.к. ось еще не является выбранной. 
{
	int i=0;
	int k=0;
	int nCount=0;
	int nPos=0;
	axis=0;
	byte d=0;
	long int ex=1;
	for (byte j=0; j<=2; j++) //смотреть MOVE в ТЗ (нужно для записи координат в разные оси))
	{
		par[j]=0;
	}
	while(i<inSize) //идем по строке
	{
		if (str[i] == ' ') 
		{
			if (d>=5) 
			{
				StSet(0,1,5);
				break;
			}
			d=1;
		}
		if((str[i] >= '0') && (str[i] <= '9')) //если встретили число
		{
			nCount=0;
			nPos=i;
			while((str[i] >= '0') && (str[i] <= '9'))
			{        
				i++;
				nCount++;
				if(i>=inSize) break;
			}
			for (int j=i-1; j>=nPos; j--)
			{
				par[d]+=(str[j]-'0')*ex;
				ex*=10;
			}
			str[k++]='*';
			d++;
			if (d>=5) {StSet(0,1,5);break;}
			ex=1;
		}
		str[k++]=str[i++];
	}
	for(int j=k; j<inSize+1; j++) str[j]='\0';
	if (par[0]>2) StSet(0,1,2); //ось не найдена(их не больше 4) 
} 

//ISR (TIMER1_COMPB_vect)
void timerIsr()
{
    // Обработчик прерывания таймера 0
	for(volatile byte engNum=0; engNum<=1; engNum++) // пройтись по всем двигателям
	{
		int stopStart = stopLong[engNum];
		long posit=enc[engNum].read()-posSw[engNum];
		if(forwardDiretion[engNum]) //смотрим направление движения
		{ //вперед
			if(posit+stopStart>=destination[engNum]) inc[engNum]=-1; //Плаынф пуск
			else inc[engNum]=1;
			if (eng[engNum].stat==4)
			{
				if((posit>prevDisPos[engNum]+disPos[engNum]) 
					&& (numDisPos[engNum]>2)) //Если текущая позиция больше предыдущей, на которой мы отсылали импульс, на disPos
				{
					digitalWrite2(pinSync,HIGH); 	//Посыл импульса на синхровыход
					prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum];  //запоминаем позицию
					digitalWrite2(pinSync,LOW);
				}
				if(posit>=destination[engNum])) //если дальше нужного положения, то останавливаем 
				{
					if(disPos[engNum]>0) //Если при остановке мы должны послать импусльс
					{
						digitalWrite2(pinSync,HIGH); //послать импульс на выход
						prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum]; //запоминаем позицию
						digitalWrite2(pinSync,LOW);
					} 
					eng[engNum].SetStat(3); //остановка
				}
			}
		}
		else 
		{ //назад
			if(posit-stopStart<=destination[engNum]) inc[engNum]=-1;	//Если пора тормозить, то будем уменьшать делитель.
			else inc[engNum]=1;											//Если не пора, то будем увеличивать.
			if(eng[engNum].stat==4)
			{
				if((posit<prevDisPos[engNum]-disPos[engNum]) 
					&& (numDisPos[engNum]>2)) //Если текущая позиция больше предыдущей, на которой мы отсылали импульс, на disPos
				{
					digitalWrite2(pinSync,HIGH); //послать импульс на выход
					prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum]; //запоминаем позицию
					digitalWrite2(pinSync,LOW);
				}
				if(posit<=destination[engNum]) //если ближе нужного положения, то останавливаем 
				{
					if(disPos[engNum]>0)	//Если при остановке мы должны послать импусльс
					{
						digitalWrite2(pinSync,HIGH); //послать импульс на выход
						prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum]; //запоминаем позицию
						digitalWrite2(pinSync,LOW);
					} 
					eng[engNum].SetStat(3); //остановка
				}
			}
		}
		statDiv[engNum]++;
		if((eng[engNum].stat)==4)  // если в движении, то едем дальше.
		{
			if(statDiv[engNum]>=divider[engNum]) //Если достиг
				eng[engNum].Move(!forwardDiretion[engNum]); //едем в нужном направлении 
		}
		if(statDiv[engNum]>divider[engNum]) statDiv[engNum]=0; //если перевалили, то сброс
		sLongDiv[engNum]++;
		if(sLongDiv[engNum]>reit[engNum][devider[engNum]]) //Если текущий такт по счёту на этом делителе > чем заданное количество тактов на этом делителе
		{
			sLongDiv[engNum]=1; //текущий такт = первый.
			if(divider[engNum]>minDivider[engNum]) divider[engNum] = divider[engNum]-inc[engNum]; 
			if(divider[engNum]<minDivider[engNum]) divider[engNum] = minDivider[engNum];
		}
		if(tagFind[engNum]) //если идет поиск опорной метки
		{
			if(forwardDiretion[engNum])
			{
				if(digitalRead2(pinOpen[engNum])) 
				{
					divider[engNum]=refDivider[engNum];
					forwardDiretion[engNum]=0;
					destination[engNum]=-9999999999999;
				}
			}
			else
			{
				if(!(digitalRead2(pinOpen[engNum]))) //при нахождении остановится и обнулить позицию
				{
					eng[engNum].SetStat(2);
					eng[engNum].pos=0;
					tagFind[engNum]=0;
				}
			}
		}
	} 
}

void loop()
{  
	Serial.flush();
	inSize=0; // Сбрасываем переменную
	memset(str1, '\0', sizeof(str1)); // Очищаем массив
	client = server.available();

	if (client)
	{  
		alreadyConnected=true;    
		if(client.available()>0)
		{        
			inSize =client.readBytesUntil('\n',str1,sizeof(str1));
			str=str1;
			if (inSize>0 && str[inSize-1]=='\r')
			{
				str[inSize-1]=0;
				inSize--;      
			}
			if(str[0]=='\r') str++;  
		}
		AnalCom();
		if (Command()) 
		{
			SetError(1); 
			client.println("Command not found!");
		}    
	}
}