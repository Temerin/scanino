
#include <arduino2.h> //Для оптимального переключения пинов
#include <Ethernet.h>
#include <Encoder.h> //Для работы с энкодерами
//#include "EncoderH.h"
#include <TimerOne.h> //таймеры
//#include <TimerThree.h> //таймеры
#include <stdarg.h> //нет необходимости. Нужно было для отладки
//int pinEncoder[4][2]={{0,0}{1,1}{2,2}{3,3}}; 
//int pinEncoder1[4]={14,24,34,44}; 
//int pinEncoder2[4]={15,25,35,45};
//pinEngine[4][3] = {{0,0,0}{1,1,1}{2,2,2}{3,3,3}};
int engPinStep[2] = {23,22};  //Распиновка. Двигатели. Каждый двигатель должен соответствовать своему энкодеру.
int engPinDir[2] = {20,24};
int engPinEn[2] = {27,46};
int pinOpen[2] = {29,28}; //Концевые выключатели.

int inSize= 0; // Переменная которая будет содержать размер буфера.
char str1[256]; // Так как типа string тут нет, будем использовать массив символов.
char* str; //Указатель на наш массив.
//boolean levelSw=false; 
byte axis; // ось. 0-ось не выбрана.
long int par[5];
long int destination[2]={0,0}; //Конечная точка.
boolean forwardDiretion[2]={1,1}; //Направление движения.
boolean tagFind[2]={0,0}; //Идет поиск опорной метки.
long posSw[2] = {0,0}; //Поправка позиции по энкодеру.
//long stopLong[4] = {7000,4000,10000,300};//Базовое расстояние торможения.
//int refLongDiv[4] = {4500,1500,1500,750}; //Базовое количество тактов таймера на каждой из частот.
//int refDivider[4] = {20,15,15,15}; //Базовый делитель частоты.
long stopLong[2] = {2000,2000};//Базовое расстояние торможения.
int refLongDiv[2] = {1000,1000}; //Базовое количество тактов таймера на каждой из частот.
int refDivider[2] = {16,16}; //Базовый делитель частоты.
int pinSync = 9;
long int disPos[2]={0,0}; //Текущий дискрет позции.
int numDisPos[2]={0,0}; //Делитель позиции, при достижении которого будет подаваться импульс на выход.
int prevDisPos[2]={0,0};

int deltaPos[2]={10,10}; //При разности текущей позиции с заданной меньше, чем на эту велечину, ось никуда не едет.



int longDiv[2] = {0,0}; //Количество тактов таймера на каждой из частот.
int divider[2] = {1,1}; //Текущий делитель.
int statDiv[2] = {0,0}; //При превышении этой переменной делителя, подается импульс на двигатель.
int sLongDiv[2] = {0,0}; //Текущий такт по счету на этой частоте.
int inc[2] = {1,1}; //Переменная для увеличения или уменьшения делителя. Принимает значения 1/-1
EthernetClient client;


byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);
//IPAddress myDns(192,168,1, 1);
//IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

EthernetServer server(400);
boolean alreadyConnected = false; // whether or not the client was connected previously





/*class EncoderH
{	
	// Переменные-члены класса
	// Устанавливаются при запуске
	int encPin[2]; // Номер пина с энкодером
	byte number; //номер энкодера
	//long oldPos; //предыдущая позиция
	//Encoder myEnc; //объект encoder
	
		// Конструктор - создает EncoderH
	// и инициализирует переменные-члены
	// и состояние
	public:
    boolean prevLevel[2]; //предыдущее состояние пинов
    boolean level[2]; //текущее состояние пинов
    long int pos; //текущая позиция
	EncoderH(int pin1, int pin2, byte num)
	{
		encPin[0] = pin1;
		encPin[1] = pin2;
		number = num; //номер энкодера в соответствии с номером двигателя
		pinMode2(encPin[0], INPUT); 
   
   
		pinMode2(encPin[1], INPUT);
		prevLevel[0] = digitalRead2(encPin[0]);     // считываем состояние 1 выхода энкодера 
		prevLevel[1] = digitalRead2(encPin[1]);     // считываем состояние 2 выхода энкодера  
		//oldPos = sOldPos;
	}
	// Текущее состояние

	void Update()
	{
		level[0] = digitalRead2(encPin[0]);     // считываем состояние 1 выхода энкодера 
		level[1] = digitalRead2(encPin[1]);     // считываем состояние 2 выхода энкодера  
    if (prevLevel[1]!=level[1])levelSw=true;
		if((!level[0]) && (prevLevel[0])){    // если состояние изменилось с положительного к нулю
			if((level[1])&&(levelSw)) 
			{
				// выход В в полож. сост., значит вращение по часовой стрелке
				pos++;  
        levelSw=false;          
			}   
			else 
			{
				// выход В в 0 сост., значит вращение против часовой стрелки     
				pos--;   
        levelSw=false;            
			} 
		}   
		prevLevel[0]=level[0];     // сохраняем значение 1 для следующего цикла 
    prevLevel[1]=level[1]; 
	}
};*/

class Engine //Класс "Двигатель"
{	
	int stepPin; //Пин для подачи импульсов.
	int dirPin; //Пин направления.
	int enPin; //Пин питания.
	int engState; //Запитан ли двигатель.
	//int mDuration; //продолжительность движения
	//int sDuration; //продолжительность остановки
	byte number; //Номер двигателя.
	boolean inv; //Инвертированно ли направление двигателя.
	unsigned long previousMillis; //Предыдущее время в миллисекундах.
	unsigned long currentMillis; // Текущее время в миллисекундах.

	public: 
    byte stat; //Битовое поле состояния.
    boolean power; //Питание (state).
    byte erCode; //Код ошибки.
	volatile long int pos; //Позиция по количеству импульсов на двигатель. В этом сканере не используется, т.к. ориентируемся на энкодеры.
  
	Engine(int pin1, int pin2, int pin3, byte num, boolean invert=0, byte st=0, byte erC=0) //распиновка, номер двигателя, направление, состояние, код ошибки
	{
		stepPin = pin1; //Присваиваем полученые пины двигателю.
		dirPin = pin2;
		enPin = pin3;
		number = num; //Даем знать двигателю свой номер. Нужно для простоты реализации некоторых возможностей.
		pinMode2(stepPin, OUTPUT); //устанавливаем пины двигателя в выходной режим.
		pinMode2(dirPin, OUTPUT);
		pinMode2(enPin, OUTPUT);
		previousMillis = millis(); //Запоминаем ориентировочное время.
		SetPower(0); //Выключаем питание двигателю (по умолчанию Arduino питает все пины)
		stat = st; //Установка поля состояния.
		erCode = erC; //Установка кода ошибки.
		power=0; //Питание выключено.
		pos=0; //Двигатель условно в нуле.
		inv=invert; //Инвертируем двигатель, если установлен наоборот.
	}


	void SetPower(boolean p) //установка питания
	{
		power = p; 
		if(p) digitalWrite2(enPin, HIGH);
		else digitalWrite2(enPin, LOW);
	}
	void SetErCode(byte erC) //установка кода ошибки
	{
		stat = 1;
		erCode = erC;
	}
	void SetStat(byte st) //установка состояния двигателя
	{
		stat = st;
	}
	void Move(boolean forDir, float m=0.1, float s=0.1) //направление (true = вперед, false = назад), задержки заданы по-умолчанию, но возможно переназначение по ситуации (Уже нет. Другая реализация.)
	{ 
		forDir=forDir ^ inv; //Если двигатель инвертирован, то направление меняется на противоположное.
		if(forDir) digitalWrite2(dirPin, LOW); //Задание направления.
		else digitalWrite2(dirPin, HIGH);
		//currentMillis = millis(); // Текущее время в миллисекундах. Фактически, сейчас не используется. Пришлось отказаться от этой задумки для большей скорости.
		if(engState == HIGH) //смена состояния stepPin (подача импульса двигателю).
		{   
			engState = LOW; // выключаем 
			digitalWrite2(stepPin, LOW);// реализуем новое состояние
		} 
		else if (engState == LOW) 
		{ 	  
			if(((forDir)&&(!inv)) || ((!forDir)&&(inv))) pos--; //отсчет движения в зависимости от направления. В этой версии - не имеет знавчения.
			else pos++;
			engState = HIGH; // выключаем 
			digitalWrite2(stepPin, HIGH); 
		} 
	}
};

/*EncoderH enc[4] = 
{
	EncoderH(14,15,1), //создание 4х энкодеров и четырех двигателей в соответствии с распиновкой.
	EncoderH(24,25,2),
	EncoderH(18,35,4),
  EncoderH(44,45,3)
};*/

Encoder enc[2] = 
{
	Encoder(3,18), //создание 4х энкодеров и четырех двигателей в соответствии с распиновкой.
	Encoder(19,2),
	//Encoder(18,16),
	//Encoder(19,17)
};

Engine eng[2] = 
{
	Engine(engPinStep[0],engPinDir[0],engPinEn[0],1,1),//распиновка, номер двигателя. 1 после номера, если направление обратное.
	Engine(engPinStep[1],engPinDir[1],engPinEn[1],2),
	//Engine(engPinStep[2],engPinDir[2],engPinEn[2],3),//направление инвертировано
	//Engine(engPinStep[3],engPinDir[3],engPinEn[3],4)
};

void setup() 
{
	pinMode2(pinSync, OUTPUT);
 // initialize the ethernet device
  Ethernet.begin(mac, ip, subnet);
  // start listening for clients
  server.begin();
  
  // Open serial communications and wait for port to open:



  
	Timer1.initialize(); //Инициализация таймера
	Timer1.setPeriod(120); //Частота таймера. Точная формула потерялась, но чем меньше значения (*), тем больше частота. 
	Timer1.attachInterrupt( timerIsr ); //Создание функции прерывания по таймеру.
	//Timer3.initialize();
	//Timer3.setPeriod(1);
	//Timer3.attachInterrupt( timerIsr2 );
 
	//Serial.begin(9600);// Открываем порт с скоростью передачи в 9600 бод(бит/с)
  
	for(byte i=0; i<2; i++) //Установка пинов концевых выключателей в состояние входа.
	{
		pinMode2(pinOpen[i], INPUT);
	}
 
	/*//------ Timer1 ----------
	TCCR1A = 0x40;;    // Режим CTC (сброс по совпадению)
	TCCR1B = 0x05;;    // Тактирование от CLK.
			  // Если нужен предделитель :
	// TCCR1B |= (1<<CS11);           // CLK/8
	// TCCR1B |= (1<<CS10)|(1<<CS11); // CLK/64
	// TCCR1B |= (1<<CS12);           // CLK/256
	// TCCR1B |= (1<<CS10)|(1<<CS12); // CLK/1024

	OCR1AH = 0x03;// Верхняя граница счета. Диапазон от 0 до 65535.
			  // Частота прерываний будет = Fclk/(N*(1+OCR1A)) = 1000Гц.
			  // где N - коэф. предделителя (1, 8, 64, 256 или 1024)
	OCR1AL= 0xE8;      
	TIMSK1 = 0x10;   // Разрешить прерывание по совпадению



	//------ Timer2 ----------
	TCCR2A = (1<<WGM21);    // Режим CTC (сброс по совпадению)
	TCCR2B = (1<<CS22);     // Тактирование от CLK.
			  // Если нужен предделитель :
	// TCCR2B = (1<<CS21);                     // CLK/8
	// TCCR2B = (1<<CS20)|(1<<CS21);           // CLK/32
	// TCCR2B = (1<<CS22);                     // CLK/64
	// TCCR2B = (1<<CS20)|(1<<CS22);           // CLK/128
	// TCCR2B = (1<<CS21)|(1<<CS22);           // CLK/256
	// TCCR2B = (1<<CS20)|(1<<CS21)|(1<<CS22); // CLK/1024

	OCR2A = 20;            // Верхняя граница счета. Диапазон от 0 до 255.
			  // Частота прерываний будет = Fclk/(N*(1+OCR2A)) = 20000 Гц.
			  // где N - коэф. предделителя (1, 8, 32, 64, 128, 256 или 1024)
	TIMSK2 = (1<<OCIE2A);   // Разрешить прерывание по совпадению

	sei ();                 // Глобально разрешить прерывания*/
}

void p(char *fmt, ... )//Нужно было для отладки. Сейчас не используется.
{
        char buf[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        client.print(buf);
}

void Idn() //далее идут команды. Смотреть ТЗ. 
{
	client.print("Model: ");
	client.println("Arduino MEGA, BIG");
	client.print("ver: ");
	client.println("5.21");
}

void StSet(byte ax=0, byte s=0, byte c=0) //дефолтно работает как Clear. 
{
	if(!ax) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetStat(s);
			eng[j].SetErCode(c);
		}
	}
	else 
	{
		eng[ax-1].SetStat(s);
		eng[ax-1].SetErCode(c);
	}
}

void EnRead(byte p=0)  //Считывание состояния питания.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
	
		client.print(eng[0].power);
		client.print(",");
		client.print(eng[1].power);
		client.print("\n");		
		//client.println(eng[3].power);
	}
	else client.println(eng[p-1].power);
}

void En(byte p=0, boolean ch=0) //Установка питания.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetPower(ch);
		}
	}
	else eng[p-1].SetPower(ch);;
}

void Stat(byte p=0) //Вывод битового поля состояния.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{		
		byte stat=eng[0].stat;
		client.print(stat);
		client.print(",");
		byte stat1=eng[1].stat;
		client.print(stat1);
		client.print("\n");
	}
	//byte stat=eng[3].stat;
	//client.println(stat);	
	else {  byte stat=eng[p-1].stat; client.println(stat); }
}

void Err(byte p=0) //Вывод ошибки
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
	
		byte code=eng[0].erCode;
		client.print(code);
		client.print(",");
		byte code1=eng[1].erCode;
		client.print(code1);
		client.print("\n");		
		//byte code=eng[3].erCode;
		//client.println(code);
	}
	else {byte code=eng[p-1].erCode; client.println(code); }
}

void Clr(byte p=0) //Команда для отчистки от ошибок и состояния. Не обязательна. Сделана для простоты поддержки кода.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{			
			eng[j].SetErCode(0);
			eng[j].SetStat(0);
		}
	}
	else 
	{
		eng[p-1].SetErCode(0);
		eng[p-1].SetStat(0);
	}
}

void SetError(byte ch, byte p=0) //Установка ошибки.
{
	if(!p) //Если ось не указано, то применяется ко всем.
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetErCode(ch);
		}
	}
	else eng[p-1].SetErCode(ch);
}

void FindTag(byte p) //поиск опорной метки
{
	if(!p) //Если ось не указано, то применяется ко всем. Может вызвать зависание, т.к. Arduino не справляется.
	{
		for (byte j=0; j<=1; j++)
		{
			bool fd=1; //Для направления движения. Можно и напрямую выставить.
			if(!(digitalRead2(pinOpen[j]))) //Если уже на концевом выключателе, то нужно отъехать от него.
			{
				fd=0; //Движемся вперед.
				destination[j]=9999999999999;
			}
			else destination[j]=-9999999999999; //Движемся назад (к концевику).
			longDiv[j]=refLongDiv[j]; //Возврат переменных разгона к эталонным.
			divider[j]=refDivider[j];
			forwardDiretion[j]=fd; //Установка направления.
			eng[j].SetStat(4); //Установка поля состояния в "Движение".
			tagFind[j]=1; //Идет поиск опорной метки.
			//destination[j]=-9999999999999;
		}
	}
	else 
	{
		bool fd=1; //Для направления движения. Можно и напрямую выставить.
		if(!(digitalRead2(pinOpen[p-1]))) //Если уже на концевом выключателе, то нужно отъехать от него.
		{
			fd=0; //Движемся вперед.
			destination[p-1]=9999999999999;
		}
		else destination[p-1]=-9999999999999;  //Движемся назад (к концевику).
		longDiv[p-1]=refLongDiv[p-1]; //Возврат переменных разгона к эталонным.
		divider[p-1]=refDivider[p-1];
		forwardDiretion[p-1]=fd; //Установка направления.
		eng[p-1].SetStat(4); //Установка поля состояния в "Движение".
		tagFind[p-1]=1; //Идет поиск опорной метки.
		//destination[p-1]=-9999999999999;
	}
}
void MoveTo(byte p) //движение с выбранной осью. Тут выставляется направление, а само движение идет по таймеру.
{
	long int posit=enc[p-1].read()-posSw[p-1]; //Считывание позиции с поправкой.
	  int bobPos=destination[p-1]-posit;
	  bool m = 0;
	  abs(bobPos);
	if(bobPos>deltaPos[p-1])//Если заданная позиция выходит за пределы дельты
	{
		longDiv[p-1]=refLongDiv[p-1]; //Возврат переменных разгона к эталонным.
		divider[p-1]=refDivider[p-1];
		if(posit>destination[p-1]) //В зависимости от текущей и заданной позиций...
		{
			if(numDisPos[p-1]>1)
			{
				if(numDisPos[p-1]>1)
				{
					disPos[p-1]=destination[p-1]-posit;
					disPos[p-1]=disPos[p-1]/(numDisPos[p-1]-1);
				}
				m=1;
				}
			forwardDiretion[p-1]=1;		//выбираем направление движения
		}
		else if(posit<destination[p-1])
		{
			if(numDisPos[p-1]>1)
			{
				if(numDisPos[p-1]>1)
				{
					disPos[p-1]=destination[p-1]+posit;
					disPos[p-1]=disPos[p-1]/(numDisPos[p-1]-1);
				}
			m=1;				
			}
			forwardDiretion[p-1]=0;			
		}
		eng[p-1].SetStat(4); //Установка поля состояния в "Движение".
		prevDisPos[p-1]=posit;
		if(m) 
		{    
			digitalWrite2(pinSync,HIGH);
			//!!!!!!!!!!!!!!!!!!!!!!! ПОСЛАТЬ ИМПУЛЬС НА ВЫХОД!!!!!!!!!
			digitalWrite2(pinSync,LOW);
		}
	}
	else eng[p-1].SetStat(3);
		
}


void MoveTo() //аналогично для всех осей
{	
  
	for (byte j=0; j<=1; j++)
	{
		longDiv[j]=refLongDiv[j]; //Возврат переменных разгона к эталонным.
		divider[j]=refDivider[j];
		long int posit=enc[j].read()-posSw[j]; //Считывание позиции с поправкой.
		int bobPos=destination[j]-posit;
		abs(bobPos);
		if(bobPos>deltaPos[j])//Если заданная позиция выходит за пределы дельты//Если заданная позиция выходит за пределы дельты
		{
			if(posit>destination[j]) //В зависимости от текущей и заданной позиций...
			{
				forwardDiretion[j]=1; //выбираем направление движения
			}
			else if(posit<destination[j])
			{
				forwardDiretion[j]=0;
			}
			eng[j].SetStat(4); //Установка поля состояния в "Движение".
		}
		else eng[j].SetStat(3);
	}
}

void Abort(byte p=0) 
{
	if(!p)
	{
		for (byte j=0; j<=1; j++)
		{
			eng[j].SetStat(0); //останавливаем все действия. Сброс.
		}
	}
	else eng[p-1].SetStat(0);
}

void Pos(byte p=0) //Вывод позиции.
{
	if(!p)
	{
		
		long int g=enc[0].read()-posSw[0]; //Считывание позиции с поправкой.
		client.print(g);
		client.print(",");
		long int g1=enc[1].read()-posSw[1]; 
		client.print(g1);
		 client.print("\n");
		//long int g=enc[3].read()-posSw[3]; //Считывание позиции с поправкой.
		//client.println(g);
	}
	else 
	{
		long int g=enc[p-1].read()-posSw[p-1]; //Считывание позиции с поправкой.
		client.println(g); 
		//client.println(divider[p-1]);
	}
}

/*void Pulse(byte p=0)
{
	disPos[0]=enc[0].read()-posSw[0];
	disPos[1]=enc[1].read()-posSw[1];
	numDisPos[0]=p;
	numDisPos[1]=p;
}*/


boolean Command() //тут происходит вызов всех команд. Лучше даже не заглядывать. 
{	
	if(par[0]>2) SetError(5);//Осей всего 4. Больше быть не должно.
	boolean error=true; //Параметр, отвечающий за успешность распознания команды.
	byte param = 0;//Если ось не выбрана.
	if (!par[0]) byte param=eng[par[0]-1].stat;//смотрим, что у нас за состояние выбронной оси.
	//client.println(str);
	//p(("%d\r\n"),str[strlen(str)-1]);
	//p(("%d\r\n"),str[0]);
	//client.println(strlen(str));
	if (param!=1) //Ось не должна быть в состоянии ошибки. Иначе некоторые команды запрещены к вводу.
	{
		if(strcmp(str,"IDN?")==0)
		{
			error=false;		
			Idn();
		}
		if((strcmp(str,"EN?")==0)||(strcmp(str,"EN*?")==0))
		{
			error=false;		
			EnRead(par[0]);			
		}
		if((strcmp(str,"EN* *")==0)||(strcmp(str,"EN* *\n")==0))//!!!!
		{
			error=false;
			//client.println(par[1]);
			if(par[1]<2) //STATE 0 или 1.
			{
				//client.println("EN set");
				En(par[0],par[1]);
			}
			else {error=false; eng[par[0]].SetErCode(4);} //ошибка 4
		}
		if((strcmp(str,"EN *,*\r")==0)||(strcmp(str,"EN *,*")==0))
		{
			error=false;
			for(int axis=0; axis<1; axis++)
			{
				if(par[axis+1]<3) //STATE 1 или 2.
				{
					//client.println("EN set");
					En(axis,par[axis+1]);
				}
				else { eng[axis].SetErCode(4);} //ошибка 4
			}
		}
		if(strcmp(str,"PULSE* *")==0)
		{
			error=false;
			numDisPos[par[0]-1]=par[1];
			disPos[par[0]-1]=0;
			
			//client.println("STAT");
			//Pusle(par[0]);
		}
		if((strcmp(str,"STAT?")==0)||(strcmp(str,"STAT*?")==0))
		{
			error=false;
			//client.println("STAT");
			Stat(par[0]);
		}
		if((strcmp(str,"ERR?")==0)||(strcmp(str,"ERR*?")==0)) //!!!!!!!!!! выводит без вызова
		{
			error=false;
			//client.println("ERR21");
			Err(par[0]);
		}
		if((strcmp(str,"CLR")==0)||(strcmp(str,"CLR*")==0))
		{
			error=false;
			//client.println("CLR");
			Clr(par[0]);			
		}
		if((strcmp(str,"MH")==0)||(strcmp(str,"MH*")==0))// двигаться, пока не достигнем опорной метки
		{
			error=false;
			//client.println("MH");
			FindTag(par[0]);
		}
		if((strcmp(str,"ABORT")==0)||(strcmp(str,"ABORT*")==0))
		{
			error=false;
			//client.println("ABORT");
			Abort(par[0]);
		}
		if((strcmp(str,"POS?")==0)||(strcmp(str,"POS*?")==0))
		{
			error=false;
			//client.println("POS");
			Pos(par[0]);
		}
		if((strcmp(str,"MOVE* *")==0))// для одной оси
		{
			error=false;
			destination[par[0]-1]=par[1];
			
			MoveTo(par[0]);
		}
		if((strcmp(str,"MOVE *,*,*,*")==0)) //для всех осей
		{
			error=false;
			for(byte g=0; g<=1; g++)
			{
				destination[g]=par[g+1];
			}	
			MoveTo();
		}
	}
	else //Команды, которые поддерживаются даже в состоянии ошибки.
	{
		if((strcmp(str,"CLR")==0)||(strcmp(str,"CLR*")==0))
		{
			error=false;	
			Clr(par[0]);			
		}
		if((strcmp(str,"ABORT")==0)||(strcmp(str,"ABORT*")==0))
		{
			error=false;	
			Abort(par[0]);
		}
		if((strcmp(str,"ERR?")==0)||(strcmp(str,"ERR*?")==0))
		{
			error=false;
			//client.println("ERR21");
			Err(par[0]);
		}
		if((strcmp(str,"STAT?")==0)||(strcmp(str,"STAT*?")==0))
		{
			error=false;
			//client.println("STAT");
			Stat(par[0]);
		}
	}
	return error;
}

void AnalCom() //кажется завершенной. Ошибки применяются ко всем осям, т.к. ось еще не является выбранной. 
{
	int i=0;
	int k=0;
	int nCount=0;
	int nPos=0;
	axis=0;
	byte d=0;
	long int ex=1;
	for (byte j=0; j<=2; j++) //смотреть MOVE в ТЗ (нужно для записи координат в разные оси))
	{
		par[j]=0;
	}
	while(i<inSize) //идем по строке
	{
		if (str[i] == ' ') {d=1; if (d>=5) {StSet(0,1,5);break;}}
		if((str[i] >= '0') && (str[i] <= '9')) //если встретили число
		{
			nCount=0;
			nPos=i;
			while((str[i] >= '0') && (str[i] <= '9'))
			{        
				i++;
				nCount++;
				if(i>=inSize) break;
			}
			for (int j=i-1; j>=nPos; j--)
			{
				par[d]+=(str[j]-'0')*ex;
				ex*=10;
			}
			str[k++]='*';
			//client.println(str);
			d++;
			if (d>=5) {StSet(0,1,5);break;}
			ex=1;
		}
		str[k++]=str[i++];
		//client.println(str[i]);
	}
	for(int j=k; j<inSize+1; j++) str[j]='\0';
	//client.println(str);
	/*for (byte j=0; j<=4; j++) //смотреть MOVE в ТЗ (нужно для записи координат в разные оси))
	{
	client.println(par[j]);;
	}*/
	if (par[0]>2) StSet(0,1,2); //ось не найдена(их не больше 4) 
} 

//ISR (TIMER1_COMPB_vect)
void timerIsr()
{
    // Обработчик прерывания таймера 0
	for(volatile byte engNum=0; engNum<=1; engNum++) // пройтись по всем двигателям
	{
		int stopStart = stopLong[engNum];
		long posit=enc[engNum].read()-posSw[engNum];
		/*if ((posit+numDisPos[engNum]>disPos[engNum])||(posit+numDisPos[engNum]>disPos[engNum])) //Если текущая позиция больше или меньше заданной на число numDisPos (N), то 
		{
			// !!!!!!!!!!!!!!!!!!!!!!!!!!Послать импульс !!!!!!!!!!!!!
			disPos[engNum]=posit; // Запомниь текущую позицию.
		}
		*/
		if(!forwardDiretion[engNum]) //смотрим направление движения
		{ //вперед
			if(posit+stopStart>=destination[engNum]) inc[engNum]=-1; //Плаынф пуск
			else inc[engNum]=1;
			if((posit>prevDisPos[engNum]+disPos[engNum]) && (numDisPos[engNum]>2) && (eng[engNum].stat==4))
			{
				digitalWrite2(pinSync,HIGH);
				//!!!!!!!!!!!!!!!!!!!!!!! ПОСЛАТЬ ИМПУЛЬС НА ВЫХОД!!!!!!!!!
				prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum];
				digitalWrite2(pinSync,LOW);
			}
			//if(divider[engNum]>refDivider[engNum]) divider[engNum]=refDivider[engNum];
			if(posit>=destination[engNum]) {if((numDisPos[engNum]) && (numDisPos[engNum]<3) && ((eng[engNum].stat)==4)){digitalWrite2(pinSync,HIGH); prevDisPos[engNum]=prevDisPos[engNum]+disPos[engNum]; digitalWrite2(pinSync,LOW);} eng[engNum].SetStat(3); } //если дальше нужного положения, то останавливаем 
		}
		else 
		{ //назад
			if(posit-stopStart<=destination[engNum]) inc[engNum]=-1;
			else inc[engNum]=1;
			if((posit<prevDisPos[engNum]-disPos[engNum]) && (numDisPos[engNum]>2) && (eng[engNum].stat==4))
			{
				digitalWrite2(pinSync,HIGH);
				//!!!!!!!!!!!!!!!!!!!!!!! ПОСЛАТЬ ИМПУЛЬС НА ВЫХОД!!!!!!!!!
				prevDisPos[engNum]=prevDisPos[engNum]-disPos[engNum];
				digitalWrite2(pinSync,LOW);
			}
			if(posit<=destination[engNum]) {if((numDisPos[engNum]) && (numDisPos[engNum]<3) && ((eng[engNum].stat)==4)){digitalWrite2(pinSync,HIGH); prevDisPos[engNum]=prevDisPos[engNum]-disPos[engNum]; digitalWrite2(pinSync,LOW);} eng[engNum].SetStat(3); } //если ближе нужного положения, то останавливаем 
		}
		statDiv[engNum]++;
		if((eng[engNum].stat)==4)  // если в движении, то едем дальше.
		{
			if(statDiv[engNum]>=divider[engNum]) eng[engNum].Move(forwardDiretion[engNum]); //едем в нужном направлении
		}
		if(statDiv[engNum]>divider[engNum]) statDiv[engNum]=0; //если перевалили, то сброс
		sLongDiv[engNum]++;
		if(sLongDiv[engNum]>longDiv[engNum]) 
		{
			sLongDiv[engNum]=1; 
			//if (divider[engNum]>3) divider[engNum]--;
			if(divider[engNum]>0) divider[engNum] = divider[engNum]-inc[engNum];
			if(divider[engNum]<1) divider[engNum] = 1;
		}
		if(tagFind[engNum]) //если идет поиск опорной метки
		{
			if(!forwardDiretion[engNum])
			{
				if(digitalRead2(pinOpen[engNum])) 
				{
					longDiv[engNum]=refLongDiv[engNum];
					divider[engNum]=refDivider[engNum];
					forwardDiretion[engNum]=1;
					//eng[engNum].SetStat(4);//!!!!!!!
					//tagFind[engNum]=1;//!!!!!!!!
					destination[engNum]=-9999999999999;
				}
			}
			//bool openP=digitalRead2(pinOpen[engNum]);
			else
			{
				if(!(digitalRead2(pinOpen[engNum]))) //при нахождении остановится и обнулить позицию
				{
					eng[engNum].SetStat(2);
					//client.println(openP);
					posSw[engNum]=enc[engNum].read();//!!!!!!!Тут иначе!!!!!
					tagFind[engNum]=0;
				}
			}
		}
	} 
}

/*void timerIsr2()
//ISR (TIMER2_COMPA_vect)
{
	// Обработчик прерывания таймера 1
	for(volatile byte engNum=0; engNum<=3; engNum++) 
	{
		enc[engNum].Update();
	}
}*/

void loop()
{  
	Serial.flush();
	//client.println("Sopa!");
	inSize=0; // Сбрасываем переменную
	memset(str1, '\0', sizeof(str1)); // Очищаем массив
	client = server.available();

	if (client)
	{  
		//client.flush();
		alreadyConnected=true;    
		if(client.available()>0)
		{        
			inSize =client.readBytesUntil('\n',str1,sizeof(str1));
			str=str1;
			// client.println(inSize);  
			// client.println(str);    
			if (inSize>0 && str[inSize-1]=='\r')
			{
				str[inSize-1]=0;
				inSize--;      
			}
			if(str[0]=='\r') str++;  
		}
		AnalCom();
		if (Command()) 
		{
			SetError(1); 
			client.println("Command not found!");
			client.println("Command not found!");
		}    
	}
}

  
//	Serial.flush();
//	//client.println("Sopa!");
//	inSize=0; // Сбрасываем переменную
//	memset(str1, '\0', sizeof(str1)); // Очищаем массив
//	if(Serial.available() > 0)
//	{
//		//delay(5); // Ждем, для того, чтобы пришли все данные !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		inSize = Serial.readBytesUntil('\n',str1,sizeof(str1));
//		str = str1;
//		if (inSize>0 && str[inSize-1]=='\r')
//		{
//			str[inSize-1]=0;
//			inSize--;
//		}
//		if(str[0]=='\r') str++;
//
//		AnalCom();
//		if (Command()) 
//		{
//			SetError(1); 
//			client.println("Command not found!");
//		}		
//	} 

